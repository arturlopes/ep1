#ifndef JOGADOR_HPP
#define JOGADOR_HPP

class Jogador {
   private:
      int vida;
      char mapaDoJogador[13][13];      

   public:
      Jogador();
      ~Jogador();

      void setVida(int vida);
      int getVida();
      void setMapaDoJogador();
      void getMapaDoJogador();
      void setElementoDoMapaDoJogador(int primeiraCoordenada, int segundaCoordenada, char elemento);
      char getElementoDoMapaDoJogador(int primeiraCoordenada, int segundaCoordenada);
};

#endif
