#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include "embarcacao.hpp"

class Submarino : public Embarcacao {
   public:
      Submarino();
      Submarino(string jogador, string primeiraCoordenada, string segundaCoordenada, string tipo, string sentido);
      ~Submarino();
};

#endif
