#ifndef CANOA_HPP
#define CANOA_HPP

#include "embarcacao.hpp"

class Canoa : public Embarcacao {
   public:
      Canoa();
      Canoa(string jogador, string primeiraCoordenada, string segundaCoordenada, string tipo, string sentido);
      ~Canoa();
};

#endif
