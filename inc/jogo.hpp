#ifndef JOGO_HPP
#define JOGO_HPP

#include "mapa.hpp"

class Jogo {
   private:
      vector<Mapa> mapa;
      int opcao;
      int escolha;
      int variavelDeParada;
      int variavelDeControle;
      int vencedor;

   public:
      Jogo();
      ~Jogo();

      void desenhoInicial();
      void escolhaDoMapa();
};

#endif
