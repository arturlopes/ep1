#ifndef PORTAAVIOES_HPP
#define PORTAAVIOES_HPP

#include "embarcacao.hpp"

class PortaAvioes : public Embarcacao {
   public:
      PortaAvioes();
      PortaAvioes(string jogador, string primeiraCoordenada, string segundaCoordenada, string tipo, string sentido);
      ~PortaAvioes();
};

#endif
