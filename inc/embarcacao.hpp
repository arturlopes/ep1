#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP

#include <string>

using namespace std;

class Embarcacao {
   private:
      string jogador;
      string primeiraCoordenada;
      string segundaCoordenada;
      string tipo;
      string sentido;

   public:
      Embarcacao();
      Embarcacao(string jogador, string primeiraCoordenada, string segundaCoordenada, string tipo, string sentido);
      ~Embarcacao();		
      
      void setJogador(string jogador);
      string getJogador();
      void setPrimeiraCoordenada(string primeiraCoordenada);
      string getPrimeiraCoordenada();
      void setSegundaCoordenada(string segundaCoordenada);
      string getSegundaCoordenada();
      void setTipo(string tipo);
      string getTipo();
      void setSentido(string sentido);
      string getSentido();

      void imprimeDados();
};

#endif
