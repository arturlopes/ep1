#ifndef MAPA_HPP
#define MAPA_HPP

#include "embarcacao.hpp"
#include "canoa.hpp"
#include "submarino.hpp"
#include "portaAvioes.hpp"
#include "jogador.hpp"

#include <vector>

class Mapa {
   private:
      Jogador jogador;
      string identificacaoDoJogador;
      vector<Canoa> canoas;
      vector<Submarino> submarinos;
      vector<PortaAvioes> portasAvioes;
      int mapa[13][13];
      char mapaDeHabilidades[13][13];

   public:
      Mapa(string jogador, int escolha);
      ~Mapa();

      void criaAsEmbarcacoes(int escolha);
      void imprimeAsEmbarcacoes();
      void setMapa();
      void getMapa();
      void setElementoDoMapa(int primeiraCoordenada, int segundaCoordenada, int elemento);
      int getElementoDoMapa(int primeiraCoordenada, int segundaCoordenada);
      void atualizaMapa(int primeiraCoordenada, int segundaCoordenada);
     
      void atualizaMapaDoJogador(int primeiraCoordenada, int segundaCoordenada, char elemento);
      void imprimeMapaDoJogador();
      void atualizaVidaDoJogador();
      int mostraVidaDoJogador();

      void setMapaDeHabilidades();
      void getMapaDeHabilidades();
      void setElementoDoMapaDeHabilidades(int primeiraCoordenada, int segundaCoordenada, char elemento);
      char getElementoDoMapaDeHabilidades(int primeiraCoordenada, int segundaCoordenada);

      int abateMissil();
};

#endif
