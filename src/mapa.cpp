#include "mapa.hpp"

#include <iostream>
#include <fstream>

using namespace std;

Mapa::Mapa(string jogador, int escolha) {

   criaAsEmbarcacoes(escolha);

   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
         mapa[i][j] = 3;
         mapaDeHabilidades[i][j] = '~';
      }
   } 

//CONSTRUÇÃO DAS CANOAS
   int primeiraCoordenada, segundaCoordenada;

   identificacaoDoJogador = jogador;

   for(unsigned int i = 0; i < canoas.size(); i++) {
      if(identificacaoDoJogador == "1") {
         if((canoas[i].getJogador()) == "1") {
            primeiraCoordenada = stoi(canoas[i].getPrimeiraCoordenada());
            segundaCoordenada = stoi(canoas[i].getSegundaCoordenada());

            setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 1);
            setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'C');
         }
      } else if(identificacaoDoJogador == "2") {
         if((canoas[i].getJogador()) == "2") {
            primeiraCoordenada = stoi(canoas[i].getPrimeiraCoordenada());
            segundaCoordenada = stoi(canoas[i].getSegundaCoordenada());

            setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 1);
            setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'C');
         }
      } else {
         cout << "ERRO!" << endl;
      }
   }

//CONSTRUÇÃO DOS SUBMARINOS
   primeiraCoordenada = 0; 
   segundaCoordenada = 0;

   for(unsigned int i = 0; i < submarinos.size(); i++) {
      if(identificacaoDoJogador == "1") {
         if((submarinos[i].getJogador()) == "1") {
            primeiraCoordenada = stoi(submarinos[i].getPrimeiraCoordenada());
            segundaCoordenada = stoi(submarinos[i].getSegundaCoordenada());

            setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 2);
            setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'S');

            if((submarinos[i].getSentido()) == "cima" || ((submarinos[i].getSentido()) == "cima\r")) {
               primeiraCoordenada--;
               
               setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 2);
               setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'S');            

            } else if((submarinos[i].getSentido()) == "baixo" || ((submarinos[i].getSentido()) == "baixo\r")) {
               primeiraCoordenada++;
               
               setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 2);
               setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'S');

            } else if((submarinos[i].getSentido()) == "esquerda" || ((submarinos[i].getSentido()) == "esquerda\r")) {
               segundaCoordenada--;
               
               setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 2);
               setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'S');

            } else if((submarinos[i].getSentido()) == "direita" || ((submarinos[i].getSentido()) == "direita\r")) {
               segundaCoordenada++;
               
               setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 2);
               setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'S');

            } else {
               cout << "ERRO!" << endl;
            }            
         }
      } else if(identificacaoDoJogador == "2") {
         if((submarinos[i].getJogador()) == "2") {
            primeiraCoordenada = stoi(submarinos[i].getPrimeiraCoordenada());
            segundaCoordenada = stoi(submarinos[i].getSegundaCoordenada());

            setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 2);
            setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'S');

            if((submarinos[i].getSentido()) == "cima" || ((submarinos[i].getSentido()) == "cima\r")) {
               primeiraCoordenada--;
               
               setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 2);
               setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'S');            

            } else if((submarinos[i].getSentido()) == "baixo" || ((submarinos[i].getSentido()) == "baixo\r")) {
               primeiraCoordenada++;
               
               setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 2);
               setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'S');

            } else if((submarinos[i].getSentido()) == "esquerda" || ((submarinos[i].getSentido()) == "esquerda\r")) {
               segundaCoordenada--;
               
               setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 2);
               setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'S');

            } else if((submarinos[i].getSentido()) == "direita" || ((submarinos[i].getSentido()) == "direita\r")) {
               segundaCoordenada++;
               
               setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 2);
               setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'S');

            } else {
               cout << "ERRO!" << endl;
            }
         }   
      }
   }

//CONSTRUÇÃO DOS PORTAS-AVIÕES   
   for(unsigned int i = 0; i < portasAvioes.size(); i++) {
      if(identificacaoDoJogador == "1") {
         if((portasAvioes[i].getJogador()) == "1") {
            primeiraCoordenada = stoi(portasAvioes[i].getPrimeiraCoordenada());
            segundaCoordenada = stoi(portasAvioes[i].getSegundaCoordenada());

            setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 1);
            setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'P');

            if((portasAvioes[i].getSentido()) == "cima" || (portasAvioes[i].getSentido()) == "cima\r") {
               for(int j = 0; j < 3; j++) {
                  primeiraCoordenada--;
               
                  setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 1);
                  setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'P');
               }            

            } else if((portasAvioes[i].getSentido()) == "baixo" || (portasAvioes[i].getSentido()) == "baixo\r") {
               for(int j = 0; j < 3; j++) {
                  primeiraCoordenada++;
               
                  setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 1);
                  setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'P');
              
               }
            } else if((portasAvioes[i].getSentido()) == "esquerda" || (portasAvioes[i].getSentido()) == "esquerda\r") {
               for(int j = 0; j < 3; j++) {
                  segundaCoordenada--;
               
                  setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 1);
                  setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'P');
               }

            } else if((portasAvioes[i].getSentido()) == "direita" || (portasAvioes[i].getSentido()) == "direita\r") {
               for(int j = 0; j < 3; j++) {
                  segundaCoordenada++;
                 
                  setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 1);
                  setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'P');
               }

            } else {
               cout << "ERRO!" << endl;
            }            
         }
      } else if(identificacaoDoJogador == "2") {
         if((portasAvioes[i].getJogador()) == "2") {
            primeiraCoordenada = stoi(portasAvioes[i].getPrimeiraCoordenada());
            segundaCoordenada = stoi(portasAvioes[i].getSegundaCoordenada());

            setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 1);
            setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'P');

            if((portasAvioes[i].getSentido()) == "cima" || (portasAvioes[i].getSentido()) == "cima\r") {
               for(int j = 0; j < 3; j++) {
                  primeiraCoordenada--;
               
                  setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 1);
                  setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'P');
               }            

            } else if((portasAvioes[i].getSentido()) == "baixo" || (portasAvioes[i].getSentido()) == "baixo\r") {
               for(int j = 0; j < 3; j++) {
                  primeiraCoordenada++;
               
                  setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 1);
                  setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'P');
              
               }
            } else if((portasAvioes[i].getSentido()) == "esquerda" || (portasAvioes[i].getSentido()) == "esquerda\r") {
               for(int j = 0; j < 3; j++) {
                  segundaCoordenada--;
               
                  setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 1);
                  setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'P');
               }

            } else if((portasAvioes[i].getSentido()) == "direita" || (portasAvioes[i].getSentido()) == "direita\r") {
               for(int j = 0; j < 3; j++) {
                  segundaCoordenada++;
                 
                  setElementoDoMapa(primeiraCoordenada, segundaCoordenada, 1);
                  setElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada, 'P');
               }

            } else {
               cout << "ERRO!" << endl;
            }            
         }
      } 
   }
}

Mapa::~Mapa() {
//   cout << "Um objeto do tipo Mapa foi destruído (Destrutor Padrão)." << endl;
}

void Mapa::criaAsEmbarcacoes(int escolha) {
   string linha = "";
   string stringAuxiliar = "";
   int variavelContadora = 0;
   vector<string> vectorDeDados;

   ifstream arquivo;

   if(escolha == 1) {
      arquivo.open("doc/map_1.txt");
   } else if(escolha == 2) {
      arquivo.open("doc/map_2.txt");
   } else if(escolha == 3) {
      arquivo.open("doc/map_3.txt");
   } else {
      cout << "ERRO!" << endl;
   }

   for(int i = 1; i <= 72; i++) { //72 == número de linhas do arquivo de texto
      getline(arquivo, linha);

      if(linha[0] == '#' || linha == "\r" || linha == "") {
         linha.clear();
         continue;
      } else {
         for(unsigned int j = 0; j < linha.size(); j++) {
            if(j == (linha.size() - 1)) {
               stringAuxiliar.push_back(linha[j]);
               vectorDeDados.push_back(stringAuxiliar);
               variavelContadora++;
               stringAuxiliar.clear();

            } else if(linha[j] == ' ') {
               vectorDeDados.push_back(stringAuxiliar);
               variavelContadora++;
	       stringAuxiliar.clear();

            } else {
	       stringAuxiliar.push_back(linha[j]);

            }

            if(variavelContadora == 4) {
               if(i >= 14 && i <= 24) {
                  canoas.push_back(*(new Canoa("1", vectorDeDados[0], vectorDeDados[1], vectorDeDados[2], vectorDeDados[3])));
               } else if(i >= 28 && i <= 34) {
                  submarinos.push_back(*(new Submarino("1", vectorDeDados[0], vectorDeDados[1], vectorDeDados[2], vectorDeDados[3])));
               } else if(i >= 38 && i <= 40) {
                  portasAvioes.push_back(*(new PortaAvioes("1", vectorDeDados[0], vectorDeDados[1], vectorDeDados[2], vectorDeDados[3])));
               } else if(i >= 46 && i <= 56) {
                  canoas.push_back(*(new Canoa("2", vectorDeDados[0], vectorDeDados[1], vectorDeDados[2], vectorDeDados[3])));
               } else if(i >= 60 && i <= 66) {
                  submarinos.push_back(*(new Submarino("2", vectorDeDados[0], vectorDeDados[1], vectorDeDados[2], vectorDeDados[3])));
               } else if(i >= 70 && i <= 72) {
                  portasAvioes.push_back(*(new PortaAvioes("2", vectorDeDados[0], vectorDeDados[1], vectorDeDados[2], vectorDeDados[3])));
               } else {
                  cout << "ERRO!" << endl;
               }

               vectorDeDados.clear();
               variavelContadora = 0;

            }
	 }
         
         linha.clear();

      } 
   }

   arquivo.close();
}

void Mapa::imprimeAsEmbarcacoes() {
   for(unsigned int i = 0; i < canoas.size(); i++) {
      canoas[i].imprimeDados();

      cout << endl;

   }

   cout << endl;
   cout << endl;

   for(unsigned int i = 0; i < submarinos.size(); i++) {
      submarinos[i].imprimeDados();

      cout << endl; 
   }

   cout << endl;
   cout << endl;

   for(unsigned int i = 0; i < portasAvioes.size(); i++) {
      portasAvioes[i].imprimeDados();

      cout << endl;

   }

   cout << endl;
   cout << endl;
}

void Mapa::setMapa() {
   int elemento;

   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
        cout << "Digite o elemento que pertencerá à posição (" << i << ", " << j << "): ";

        scanf("%d", &elemento);

        mapa[i][j] = elemento;  

      }
   }
}

void Mapa::getMapa() {
   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
         if(j == 12) {
            cout << mapa[i][j] << endl;

         } else {
            cout << mapa[i][j];

         }
      }
   }
}

void Mapa::atualizaMapa(int primeiraCoordenada, int segundaCoordenada) {
   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
        mapa[primeiraCoordenada][segundaCoordenada] -= 1;
      }
   } 
}

void Mapa::setElementoDoMapa(int primeiraCoordenada, int segundaCoordenada, int elemento) {
   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
        mapa[primeiraCoordenada][segundaCoordenada] = elemento;
      }
   }
}

int Mapa::getElementoDoMapa(int primeiraCoordenada, int segundaCoordenada) {
   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
         return mapa[primeiraCoordenada][segundaCoordenada];
      }
   }
}

void Mapa::imprimeMapaDoJogador() {
   if(identificacaoDoJogador == "1") {
      cout << endl << "MAPA DO JOGADOR 1: \n" << endl;

      jogador.getMapaDoJogador();

      cout << endl;
   } else if(identificacaoDoJogador == "2") {
      cout << endl << "MAPA DO JOGADOR 2: \n" << endl;

      jogador.getMapaDoJogador();

      cout << endl;
   } else {
      cout << "ERRO!" << endl;
   }
}

void Mapa::atualizaMapaDoJogador(int primeiraCoordenada, int segundaCoordenada, char elemento) {
   jogador.setElementoDoMapaDoJogador(primeiraCoordenada, segundaCoordenada, elemento);
}

int Mapa::mostraVidaDoJogador() {
   return jogador.getVida();
}

void Mapa::atualizaVidaDoJogador() {
   jogador.setVida(((jogador.getVida()) - 1));
}

void Mapa::setMapaDeHabilidades() {
   char elemento;

   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
        cout << "Digite o elemento que pertencerá à posição (" << i << ", " << j << "): ";
        scanf(" %c", &elemento);

        mapa[i][j] = elemento;  
      }
   }
}
     
void Mapa::getMapaDeHabilidades() {
   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
         if(j == 12) {
            cout << mapaDeHabilidades[i][j] << endl;
         } else {
            cout << mapaDeHabilidades[i][j];
         }
      }
   }
}

void Mapa::setElementoDoMapaDeHabilidades(int primeiraCoordenada, int segundaCoordenada, char elemento) {
   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
        mapaDeHabilidades[primeiraCoordenada][segundaCoordenada] = elemento;
      }
   }
}

char Mapa::getElementoDoMapaDeHabilidades(int primeiraCoordenada, int segundaCoordenada) {
   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
         return mapaDeHabilidades[primeiraCoordenada][segundaCoordenada];
      }
   }
}
 
int Mapa::abateMissil() {
   return (rand() % 2);
}
