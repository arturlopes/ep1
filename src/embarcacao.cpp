#include "embarcacao.hpp"

#include <iostream>

Embarcacao::Embarcacao() {
   jogador = "";
   primeiraCoordenada = "";
   segundaCoordenada = "";
   tipo = "";
   sentido = "";

//   cout << "Um objeto do tipo Embarcacao foi construído (Construtor Padrão)." << endl;
}
Embarcacao::Embarcacao(string jogador, string primeiraCoordenada, string segundaCoordenada, string tipo, string sentido) {
   this->jogador = jogador;
   this->primeiraCoordenada = primeiraCoordenada;
   this->segundaCoordenada = segundaCoordenada;
   this->tipo = tipo;
   this->sentido = sentido;

//   cout << "Um objeto do tipo Embarcacao foi construído (Construtor c/ Parâmetros)." << endl;
}

Embarcacao::~Embarcacao() {
//   cout << "Um objeto do tipo Embarcacao foi destruído (Destrutor Padrão)." << endl;
}		

void Embarcacao::setJogador(string jogador) {
   this->jogador = jogador;
}

string Embarcacao::getJogador() {
   return jogador;
}

void Embarcacao::setPrimeiraCoordenada(string primeiraCoordenada) {
   this->primeiraCoordenada = primeiraCoordenada;
}

string Embarcacao::getPrimeiraCoordenada() {
   return primeiraCoordenada;
}

void Embarcacao::setSegundaCoordenada(string segundaCoordenada) {
   this->segundaCoordenada = segundaCoordenada;
}

string Embarcacao::getSegundaCoordenada() {
   return segundaCoordenada;
}

void Embarcacao::setTipo(string tipo) {
   this->tipo = tipo;
}

string Embarcacao::getTipo() {
   return tipo;
}

void Embarcacao::setSentido(string sentido) {
   this->sentido = sentido;
}

string Embarcacao::getSentido() {
   return sentido;
}

void Embarcacao::imprimeDados() {
   cout << "Jogador:        " << jogador << endl;
   cout << "1.ª coordenada: " << primeiraCoordenada << endl;
   cout << "2.ª coordenada: " << segundaCoordenada << endl;
   cout << "Tipo:           " << tipo << endl;
   cout << "Sentido:        " << sentido << endl; 
}
