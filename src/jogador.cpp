#include "jogador.hpp"

#include <iostream>

using namespace std;

Jogador::Jogador() {
   vida = 22;

   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
         mapaDoJogador[i][j] = ' ';
      }
   }

//   cout << "Um objeto do tipo Jogador foi construído (Construtor Padrão)." << endl;
}

Jogador::~Jogador() {
//   cout << "Um objeto do tipo Jogador foi destruído (Destrutor Padrão)." << endl;
}

void Jogador::setVida(int vida) {
   this->vida = vida;
}

int Jogador::getVida() {
   return vida;
}

void Jogador::setMapaDoJogador() {
   char elemento;

   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
        cout << "Digite o elemento que pertencerá à posição (" << i << ", " << j << "): ";
        scanf(" %c", &elemento);

        mapaDoJogador[i][j] = elemento;
      }
   }
}

void Jogador::getMapaDoJogador() {
   for(int i = 0; i < 1; i++) {
      for(int j = 0; j < 13; j++) {
         if(j == 0) {
            cout << "      " << j;
         } else if(j == 12) {
            cout << "  " << j << endl;
         } else {
            if(j > 9) {
               cout << "  " << j;
            } else {
               cout << "   " << j;
            }
         }
      }
   }

   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
         if(j == 0) {
            if(i < 10) {
               cout << "  " << i;
            } else {
               cout << " " << i;
            }
         }
         if(j == 0) {
            cout << "| " << mapaDoJogador[i][j];
         } else if(j == 12) {
            cout << " | " << mapaDoJogador[i][j] << " |" << endl;
         } else {
            cout << " | " << mapaDoJogador[i][j];
         }
      }
   } 
}

void Jogador::setElementoDoMapaDoJogador(int primeiraCoordenada, int segundaCoordenada, char elemento) {
   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
        mapaDoJogador[primeiraCoordenada][segundaCoordenada] = elemento;
      }
   }
}

char Jogador::getElementoDoMapaDoJogador(int primeiraCoordenada, int segundaCoordenada) {
   for(int i = 0; i < 13; i++) {
      for(int j = 0; j < 13; j++) {
         return mapaDoJogador[primeiraCoordenada][segundaCoordenada];
      }
   }
}
