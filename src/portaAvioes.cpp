#include "embarcacao.hpp"
#include "portaAvioes.hpp"

PortaAvioes::PortaAvioes() {
   setPrimeiraCoordenada("");
   setSegundaCoordenada("");
   setTipo("");
   setSentido("");

//   cout << "Um objeto do tipo PortaAvioes foi construído (Construtor Padrão)." << endl;
}

PortaAvioes::PortaAvioes(string jogador, string primeiraCoordenada, string segundaCoordenada, string tipo, string sentido) {
   setJogador(jogador);
   setPrimeiraCoordenada(primeiraCoordenada);
   setSegundaCoordenada(segundaCoordenada);
   setTipo(tipo);
   setSentido(sentido);
   
//   cout << "Um objeto do tipo PortaAvioes foi construído (Construtor c/ Parâmetros)." << endl;
}

PortaAvioes::~PortaAvioes() {
//   cout << "Um objeto do tipo PortaAvioes foi destruído (Destrutor Padrão)." << endl;
}
