#include "embarcacao.hpp"
#include "canoa.hpp"

Canoa::Canoa() {
   setJogador("");
   setPrimeiraCoordenada("");
   setSegundaCoordenada("");
   setTipo("");
   setSentido("");

//   cout << "Um objeto do tipo Canoa foi construído (Construtor Padrão)." << endl;
}

Canoa::Canoa(string jogador, string primeiraCoordenada, string segundaCoordenada, string tipo, string sentido) {
   setJogador(jogador);
   setPrimeiraCoordenada(primeiraCoordenada);
   setSegundaCoordenada(segundaCoordenada);
   setTipo(tipo);
   setSentido(sentido);
   
//   cout << "Um objeto do tipo Canoa foi construído (Construtor c/ Parâmetros)." << endl;
}

Canoa::~Canoa() {
//   cout << "Um objeto do tipo Canoa foi destruído (Destrutor Padrão)." << endl;
}
