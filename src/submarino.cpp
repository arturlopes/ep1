#include "embarcacao.hpp"
#include "submarino.hpp"

Submarino::Submarino() {
   setPrimeiraCoordenada("");
   setSegundaCoordenada("");
   setTipo("");
   setSentido("");

//   cout << "Um objeto do tipo Submarino foi construído (Construtor Padrão)." << endl;
}

Submarino::Submarino(string jogador, string primeiraCoordenada, string segundaCoordenada, string tipo, string sentido) {
   setJogador(jogador);
   setPrimeiraCoordenada(primeiraCoordenada);
   setSegundaCoordenada(segundaCoordenada);
   setTipo(tipo);
   setSentido(sentido);
   
//   cout << "Um objeto do tipo Submarino foi construído (Construtor c/ Parâmetros)." << endl;
}

Submarino::~Submarino() {
//   cout << "Um objeto do tipo Submarino foi destruído (Destrutor Padrão)." << endl;
}
