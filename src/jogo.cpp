#include "jogo.hpp"

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

Jogo::Jogo() {
   variavelDeParada = 0;
   variavelDeControle = 1;
   vencedor = 0;

   while(true) {
      while(1) {
         desenhoInicial();        
 
         cout << endl << "1 - INICIAR PARTIDA" << endl;
         cout << "0 - SAIR" << endl << endl;

         cout << "Digite a opção desejada [use apenas '0' ou '1'] e tecle <Enter>: ";    

         scanf("%1d", &opcao);

         if(opcao == 1) {
            escolhaDoMapa();

            system("clear");

            while(vencedor == 0) {
               if((variavelDeControle % 2) != 0) {
                  mapa[1].imprimeMapaDoJogador();
                  
                  int primeiraCoordenada, segundaCoordenada;
                  cout << endl << "VEZ DO JOGADOR 1!\n" << "Informe as coordenadas, uma de cada vez, para realizar o ataque." << endl;
                  cout << "Digite o número da linha [use números entre 0-12, incluindo estes] e tecle <Enter>: ";
                  scanf("%d", &primeiraCoordenada);
                  cout << "Digite o número da coluna [use números entre 0-12, includindo estes] e tecle <Enter>: ";
                  scanf("%d", &segundaCoordenada);

                  if((mapa[1].getElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada)) == 'P') {
                     if(mapa[1].abateMissil() == 0) {
                        if((mapa[1].getElementoDoMapa(primeiraCoordenada, segundaCoordenada)) == 3) {
                           mapa[1].atualizaMapaDoJogador(primeiraCoordenada, segundaCoordenada, '~');

                           variavelDeControle++;                           

                           system("clear");
                            
                           cout << endl << "Tiro na água!" << endl;

                           mapa[1].imprimeMapaDoJogador();
 
                        } else if((mapa[1].getElementoDoMapa(primeiraCoordenada, segundaCoordenada)) == 0) {
                           system("clear");

                           cout << endl << "Você já destruiu a embarcação que se encontrava nesta posição. Tente novamente!" << endl;
                           continue;

                        } else if((mapa[1].getElementoDoMapa(primeiraCoordenada, segundaCoordenada)) == 2) {
                           mapa[1].atualizaMapa(primeiraCoordenada, segundaCoordenada);
                           mapa[1].atualizaMapaDoJogador(primeiraCoordenada, segundaCoordenada, 'x');
                           mapa[1].atualizaVidaDoJogador();

                           system("clear");

                           cout << endl << "Embarcação atingida!" << endl;

                           if((mapa[1].mostraVidaDoJogador()) == 0) {
                              vencedor = 1;
                             
                           }

                           continue;                         
                           
                        } else {
                           mapa[1].atualizaMapa(primeiraCoordenada, segundaCoordenada);
                           mapa[1].atualizaMapaDoJogador(primeiraCoordenada, segundaCoordenada, 'X');
                           mapa[1].atualizaVidaDoJogador();

                           system("clear");

                           cout << endl << "Embarcação atingida!" << endl;

                           if((mapa[1].mostraVidaDoJogador()) == 0) {
                              vencedor = 1;
                             
                           }

                           continue;

                        }
                     } else {
                        variavelDeControle++;

                        system("clear");

                        cout << endl << "Míssil abatido!" << endl;

                     }                    
                  } else if((mapa[1].getElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada)) == '~') {
                     mapa[1].atualizaMapaDoJogador(primeiraCoordenada, segundaCoordenada, '~');

                     variavelDeControle++;
                    
                     system("clear");

                     cout << endl << "Tiro na água!" << endl;

                     mapa[1].imprimeMapaDoJogador(); 
 
                  } else {
                     if((mapa[1].getElementoDoMapa(primeiraCoordenada, segundaCoordenada)) == 3) {
                           mapa[1].atualizaMapaDoJogador(primeiraCoordenada, segundaCoordenada, '~');
                         
                           variavelDeControle++;
 
                           system("clear");
       
                           cout << endl << "Tiro na água!" << endl;

                           mapa[1].imprimeMapaDoJogador();
 
                        } else if((mapa[1].getElementoDoMapa(primeiraCoordenada, segundaCoordenada)) == 0) {
                           system("clear");
                         
                           cout << endl << "Você já destruiu a embarcação que se encontrava nesta posição. Tente novamente!" << endl;
                           
                           continue;

                        } else if((mapa[1].getElementoDoMapa(primeiraCoordenada, segundaCoordenada)) == 2) {
                           mapa[1].atualizaMapa(primeiraCoordenada, segundaCoordenada);
                           mapa[1].atualizaMapaDoJogador(primeiraCoordenada, segundaCoordenada, 'x');
                           mapa[1].atualizaVidaDoJogador();
 
                           system("clear");

                           cout << endl << "Embarcação atingida!" << endl;

                           if((mapa[1].mostraVidaDoJogador()) == 0) {
                              vencedor = 1;
                             
                           }
                          
                           continue;                         

                        } else {
                           mapa[1].atualizaMapa(primeiraCoordenada, segundaCoordenada);
                           mapa[1].atualizaMapaDoJogador(primeiraCoordenada, segundaCoordenada, 'X');
                           mapa[1].atualizaVidaDoJogador();
                          
                           system("clear");

                           cout << endl << "Embarcação atingida!" << endl;

                           if((mapa[1].mostraVidaDoJogador()) == 0) {
                              vencedor = 1;
                             
                           }

                           continue;

                        }
                  }

                  if((mapa[1].mostraVidaDoJogador()) == 0) {
                     vencedor = 1;

                  }
//
               } else {
                  mapa[0].imprimeMapaDoJogador();
                  
                  int primeiraCoordenada, segundaCoordenada;
                  cout << endl << "VEZ DO JOGADOR 2!\n" << "Informe as coordenadas, uma de cada vez, para realizar o ataque." << endl;
                  cout << "Digite o número da linha [use números entre 0-12, incluindo estes] e tecle <Enter>: ";
                  scanf("%d", &primeiraCoordenada);
                  cout << "Digite o número da coluna [use números entre 0-12, incluindo estes] e tecle <Enter>: ";
                  scanf("%d", &segundaCoordenada);

                  if((mapa[0].getElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada)) == 'P') {
                     if(mapa[0].abateMissil() == 0) {
                        if((mapa[0].getElementoDoMapa(primeiraCoordenada, segundaCoordenada)) == 3) {
                           mapa[0].atualizaMapaDoJogador(primeiraCoordenada, segundaCoordenada, '~');

                           variavelDeControle++;
                           
                           system("clear");

                           cout << endl << "Tiro na água!" << endl;

                           mapa[0].imprimeMapaDoJogador();
 
                        } else if((mapa[0].getElementoDoMapa(primeiraCoordenada, segundaCoordenada)) == 0) {
                           system("clear");

                           cout << endl << "Você já destruiu a embarcação que se encontrava nesta posição. Tente novamente!" << endl;

                           continue;

                        } else if((mapa[0].getElementoDoMapa(primeiraCoordenada, segundaCoordenada)) == 2) {
                           mapa[0].atualizaMapa(primeiraCoordenada, segundaCoordenada);
                           mapa[0].atualizaMapaDoJogador(primeiraCoordenada, segundaCoordenada, 'x');
                           mapa[0].atualizaVidaDoJogador();

                           system("clear");

                           cout << endl << "Embarcação atingida!" << endl;

                           if((mapa[0].mostraVidaDoJogador()) == 0) {
                              vencedor = 2;
                             
                           }

                           continue;

                        } else {
                           mapa[0].atualizaMapa(primeiraCoordenada, segundaCoordenada);
                           mapa[0].atualizaMapaDoJogador(primeiraCoordenada, segundaCoordenada, 'X');
                           mapa[0].atualizaVidaDoJogador();

                           system("clear");

                           cout << endl << "Embarcação atingida!" << endl;                           

                           if((mapa[0].mostraVidaDoJogador()) == 0) {
                              vencedor = 2;
                             
                           }

                           continue;
 
                        }
                     } else {
                        variavelDeControle++; 
 
                        system("clear");

                        cout << endl << "Míssil abatido!" << endl;
 
                     } 
                  } else if((mapa[0].getElementoDoMapaDeHabilidades(primeiraCoordenada, segundaCoordenada)) == '~') {
                     mapa[0].atualizaMapaDoJogador(primeiraCoordenada, segundaCoordenada, '~');
                   
                     variavelDeControle++;
 
                     system("clear");
        
                     cout << endl << "Tiro na água!" << endl;

                     mapa[0].imprimeMapaDoJogador();                    
 
                  } else {
                     if((mapa[0].getElementoDoMapa(primeiraCoordenada, segundaCoordenada)) == 3) {
                           mapa[0].atualizaMapaDoJogador(primeiraCoordenada, segundaCoordenada, '~');
                         
                           variavelDeControle++;
 
                           system("clear");

                           cout << endl << "Tiro na água!" << endl;

                           mapa[0].imprimeMapaDoJogador();
 
                        } else if((mapa[0].getElementoDoMapa(primeiraCoordenada, segundaCoordenada)) == 0) {
                           system("clear");

                           cout << endl << "Você já destruiu a embarcação que se encontrava nesta posição. Tente novamente!" << endl;

                           continue;

                        } else if((mapa[0].getElementoDoMapa(primeiraCoordenada, segundaCoordenada)) == 2) {
                           mapa[0].atualizaMapa(primeiraCoordenada, segundaCoordenada);
                           mapa[0].atualizaMapaDoJogador(primeiraCoordenada, segundaCoordenada, 'x');
                           mapa[0].atualizaVidaDoJogador();
                           
                           system("clear");

                           cout << endl << "Embarcação atingida!" << endl;

                           if((mapa[0].mostraVidaDoJogador()) == 0) {
                              vencedor = 2;
                             
                           }
 
                           continue;

                        } else {
                           mapa[0].atualizaMapa(primeiraCoordenada, segundaCoordenada);
                           mapa[0].atualizaMapaDoJogador(primeiraCoordenada, segundaCoordenada, 'X');
                           mapa[0].atualizaVidaDoJogador();

                           system("clear");

                           cout << endl << "Embarcação atingida!" << endl;

                           if((mapa[0].mostraVidaDoJogador()) == 0) {
                              vencedor = 2;
                             
                           }
                        
                           continue;                         
 
                        }
                  }
               }
            } 
 
            if(vencedor == 1) {
               cout << endl << "~ O Jogador 1 venceu a partida! ~" << endl;

               cout << endl << "PARTIDA ENCERRADA.\n" << endl;

               mapa.clear();

               variavelDeParada = 0;

            }

            if(vencedor == 2) {
               cout << endl << "~ O Jogador 2 venceu a partida! ~" << endl;
               
               cout << endl << "PARTIDA ENCERRADA.\n" << endl;

               mapa.clear();

               variavelDeParada = 0;

            }
 
         } else if(opcao == 0) {
            system("clear");

            cout << "Volte sempre!" << endl;
            variavelDeParada = 1;

            break;

         } else {
            system("clear");

            cout << endl << "Opção inválida! Tecle \"1\" para INICIAR PARTIDA ou \"0\" para SAIR." << endl;

            continue;

         }
      }
 
      //
      
      if(variavelDeParada == 1) {
         break;

      } else if(variavelDeParada == 0) {
         continue;

      }
   }     
}

Jogo::~Jogo() {
//Destrutor Padrão
}

void Jogo::desenhoInicial() {
   cout << endl;
   cout << "                           ^                           " << endl;
   cout << "                          /|\\                          " << endl;
   cout << "                         / | \\                         " << endl;
   cout << "                        /  |  \\                        " << endl;
   cout << "                       /   |   \\                       " << endl;
   cout << "                      /____|____\\                      " << endl;
   cout << "               ____________|____________               " << endl;
   cout << "           ~~~ \\_______________________/ ~~~           " << endl;
   cout << "        ~~~~~~~ \\_____________________/ ~~~~~~~        " << endl;
   cout << "         ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~         " << endl;              
   cout << "      ~~~~~~~~~~~~~ |B|A|T|A|L|H|A| ~~~~~~~~~~~~~      " << endl;
   cout << "       ~~~~~~~~~~~~~~ |N|A|V|A|L| ~~~~~~~~~~~~~~       " << endl;
   cout << "    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    " << endl;
   cout << "     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~     " << endl;
   cout << "  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  " << endl; 
   cout << "   ~~~~~ EP1 - 1.º/2019 | Orientação a Objetos ~~~~~   " << endl;
   cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
}

void Jogo::escolhaDoMapa() {
   while(1) {
      cout << endl << "ESCOLHA UM MAPA: \n" << endl;
      cout << "1 - 1.º MAPA" << endl;
      cout << "2 - 2.º MAPA" << endl;
      cout << "3 - 3.º MAPA" << endl;
      cout << "4 - ESCOLHA ALEATÓRIA" << endl << endl;

      cout << "Digite a opção desejada [use números entre 1-4, incluindo estes] e tecle <ENTER>: ";

      scanf("%1d", &escolha);

      if(escolha == 1) {
         mapa.push_back(*(new Mapa("1", 1)));
         mapa.push_back(*(new Mapa("2", 1)));

         break;

      } else if(escolha == 2) {
         mapa.push_back(*(new Mapa("1", 2)));
         mapa.push_back(*(new Mapa("2", 2)));
         
         break;

      } else if(escolha == 3) {
         mapa.push_back(*(new Mapa("1", 3)));
         mapa.push_back(*(new Mapa("2", 3)));

         break;

      } else if(escolha == 4) {
         mapa.push_back(*(new Mapa("1", ((rand() % 3) + 1))));
         mapa.push_back(*(new Mapa("2", ((rand() % 3) + 1))));

         break;
      } else {
         system("clear"); 

         cout << endl << "Opção invália! Faça uma escolha entre um dos três mapas disponíveis." << endl;
  
         continue;
   
      }
   }
}
