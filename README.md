# EP1 - OO 2019.1 (UnB - Gama)

&nbsp;&nbsp;Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval), onde todas suas instruções, dicas e requisitos para a avaliação estão sendo descritos na [wiki](https://gitlab.com/oofga/eps/eps_2019_1/ep1/wikis/home) do repositório.

## Instruções

&nbsp;&nbsp;Recomenda-se construir o corpo do README com no mínimo no seguinte formato:

* Descrição do projeto
* Instruções de execução

&nbsp;&nbsp;De forma básica, a construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Caso já tenha, posteriormente a uma mudança, criado os objetos definidos, lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```
Vale ressaltar que a construção e o funcionamento do projeto depende única e exclusivamente da formatação dos aquivos de texto (os mapas) e, também, dos inputs corretos do usuário. Portanto, pede-se que os arquivos de texto sigam o padrão definido e, então, pré-estabelecidos na pasta "/doc". O espaçamento de um número "1" de linhas entre cada uma das linhas do arquivo de texto (com execeção do cabeçalho, que também deve seguir o parão pré-estabelecido) contendo as informações das embarcações (primeira coordenada, segunda coodenada, tipo de embarcação e sentido/direção) é essencial para o funcionamento da lógica do programa que vai utilizar cada detalhe do mapa para varrer o mesmo e extrair as informações necessárias. Caso se deseje modificar os aquivos de texto, é necessário atualizar e informar (no método 'void criaAsEmbarcacoes(int escolha)', da classe Mapa) o intervalo no qual será possível identificar e extrair as informações citadas. Já sobre os possíveis problemas com o usuário, foram inseridas informações acerca do funcionamento do jogo durante a execução do próprio jogo, o que, se seguidas, não implicará em erros.

## Observações

* Fique avontade para manter o README da forma que achar melhor, mas é importante manter ao menos as informações descritas acima para, durante a correção, facilitar a avaliação.
* Divirtam-se mas sempre procurando atender aos critérios de avaliação informados na Wiki, ela definirá a nota a ser atribuída ao projeto.
